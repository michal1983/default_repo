package com.cs.fx.validation.validators;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cs.fx.transaction.TradeInput;
import com.cs.fx.transaction.TradeValidationOutput;

/**
 * couple of basic test cases  
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations =
{ "classpath:validationServiceContext.xml" })
public class AllValidationTest {

	@Autowired
	@Qualifier(value="allValidation")
	private AllValidation allValidation;
	
	@Test
	public void checkIsWeekend_saturday() throws ParseException
	{
		Date staurdayDate = Validation.FORMATTER.parse("2017-10-21");		
		boolean result = allValidation.isWeekend(staurdayDate);
		assertTrue("staurday true should be returned", result);
	}
	
	@Test
	public void checkIsWeekend_friday() throws ParseException
	{
		Date staurdayDate = Validation.FORMATTER.parse("2017-10-20");		
		boolean result = allValidation.isWeekend(staurdayDate);
		assertFalse("friday false should be returned", result);
	}
	
	@Test
	public void checkValidateTradeDate_valueDateBeforeTradeDate() throws ParseException
	{
		TradeInput tradeInput = new TradeInput();
		tradeInput.setValueDate("2017-10-20");
		tradeInput.setTradeDate("2017-10-21");
		TradeValidationOutput tradeValidationOutput = new TradeValidationOutput(tradeInput);		
		allValidation.validateTradeDate(tradeValidationOutput);
		assertFalse("an error should be returned", tradeValidationOutput.getErrors().isEmpty());
	}
	
	@Test
	public void checkValidateTradeDate_valueDateAfterTradeDate() throws ParseException
	{
		TradeInput tradeInput = new TradeInput();
		tradeInput.setValueDate("2017-10-21");
		tradeInput.setTradeDate("2017-10-20");
		TradeValidationOutput tradeValidationOutput = new TradeValidationOutput(tradeInput);		
		allValidation.validateTradeDate(tradeValidationOutput);
		assertTrue("no error should be returned", tradeValidationOutput.getErrors().isEmpty());
	}
	
	@Test
	public void checkValidateCounterParties_wrongCounterParty() throws ParseException
	{
		TradeInput tradeInput = new TradeInput();
		tradeInput.setCustomer("wrong_counter_partie");
		TradeValidationOutput tradeValidationOutput = new TradeValidationOutput(tradeInput);		
		allValidation.validateCounterParties(tradeValidationOutput);
		assertFalse("an error should be returned", tradeValidationOutput.getErrors().isEmpty());
	}
	
	@Test
	public void checkValidateCounterParties_correctCounterParty() throws ParseException
	{
		TradeInput tradeInput = new TradeInput();
		tradeInput.setCustomer("PLUTO1");
		TradeValidationOutput tradeValidationOutput = new TradeValidationOutput(tradeInput);		
		allValidation.validateCounterParties(tradeValidationOutput);
		assertTrue("no error should be returned", tradeValidationOutput.getErrors().isEmpty());
	}
	
	@Test
	public void checkValidateCcy_wrongCcy() throws ParseException
	{
		TradeInput tradeInput = new TradeInput();
		tradeInput.setCcy("PLN1");
		TradeValidationOutput tradeValidationOutput = new TradeValidationOutput(tradeInput);		
		allValidation.validateCcy(tradeValidationOutput);
		assertFalse("an error should be returned", tradeValidationOutput.getErrors().isEmpty());
	}
	
	@Test
	public void checkValidateCcyPair_wrongCcyPair() throws ParseException
	{
		TradeInput tradeInput = new TradeInput();
		tradeInput.setCcyPair("AAABBB");
		TradeValidationOutput tradeValidationOutput = new TradeValidationOutput(tradeInput);		
		allValidation.validateCcyPair(tradeValidationOutput);
		assertFalse("an error should be returned", tradeValidationOutput.getErrors().isEmpty());
	}

}
