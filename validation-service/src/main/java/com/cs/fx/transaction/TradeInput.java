package com.cs.fx.transaction;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TradeInput {

	@JsonProperty("TCN")
	private String tcn;
	private String customer;
	private String ccyPair;
	private String type;
	private String style;
	private String direction;
	private String strategy;
	private String tradeDate;
	private BigDecimal amount1;
	private BigDecimal amount2;
	private BigDecimal rate;
	private String valueDate;
	private String legalEntity;
	private String trader;
	private String deliveryDate;
	private String expiryDate;
	private String excerciseStartDate;
	
	private String premiumCcy;
	private String payCcy;
	private String premium;
	private String premiumType;
	private String premiumDate;
	
	@JsonProperty("CCY")
	private String ccy;
	
	@JsonProperty("PV")
	private String pv;
	

	public TradeInput() {
		super();
	}

	public String getTcn() {
		return tcn;
	}

	public void setTcn(String tcn) {
		this.tcn = tcn;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCcyPair() {
		return ccyPair;
	}

	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public BigDecimal getAmount1() {
		return amount1;
	}

	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}

	public BigDecimal getAmount2() {
		return amount2;
	}

	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public String getLegalEntity() {
		return legalEntity;
	}

	public void setLegalEntity(String legalEntity) {
		this.legalEntity = legalEntity;
	}

	public String getTrader() {
		return trader;
	}

	public void setTrader(String trader) {
		this.trader = trader;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	
	public String getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
	
	

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getExcerciseStartDate() {
		return excerciseStartDate;
	}

	public void setExcerciseStartDate(String excerciseStartDate) {
		this.excerciseStartDate = excerciseStartDate;
	}

	public String getPremiumCcy() {
		return premiumCcy;
	}

	public void setPremiumCcy(String premiumCcy) {
		this.premiumCcy = premiumCcy;
	}

	public String getPayCcy() {
		return payCcy;
	}

	public void setPayCcy(String payCcy) {
		this.payCcy = payCcy;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getPremiumType() {
		return premiumType;
	}

	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType;
	}

	public String getPremiumDate() {
		return premiumDate;
	}

	public void setPremiumDate(String premiumDate) {
		this.premiumDate = premiumDate;
	}
	
	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getPv() {
		return pv;
	}

	public void setPv(String pv) {
		this.pv = pv;
	}

	@Override
	public String toString() {
		return "TransactionInput [tcn=" + tcn + ", customer=" + customer + ", ccyPair=" + ccyPair + ", type=" + type
				+ ", style=" + style + ", direction=" + direction + ", tradeDate=" + tradeDate + ", amount1=" + amount1
				+ ", amount2=" + amount2 + ", rate=" + rate + ", valueDate=" + valueDate + ", legalEntity="
				+ legalEntity + ", trader=" + trader + "]";
	}
}
