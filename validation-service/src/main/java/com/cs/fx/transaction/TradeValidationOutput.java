package com.cs.fx.transaction;

import java.util.ArrayList;
import java.util.List;

import com.cs.fx.validation.ValidationError;

/**
 * 
 * Stores {@link TradeInput} and validation results
 *
 */

public class TradeValidationOutput {

	private TradeInput tradeInput;
	private boolean valid;
	private List<ValidationError> errors;
	
	public TradeValidationOutput(TradeInput tradeInput) {
		super();
		this.tradeInput = tradeInput;
		this.valid = true;
		this.errors = new ArrayList<ValidationError>();
	}
	
	
	public TradeInput getTradeInput() {
		return tradeInput;
	}
	public void setTradeInput(TradeInput transactionInput) {
		this.tradeInput = transactionInput;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public List<ValidationError> getErrors() {
		return errors;
	}
	public void setErrors(List<ValidationError> errors) {
		this.errors = errors;
	}
}
