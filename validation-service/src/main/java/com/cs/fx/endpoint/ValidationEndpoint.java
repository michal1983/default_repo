package com.cs.fx.endpoint;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.fx.transaction.TradeInput;
import com.cs.fx.transaction.TradeValidationOutput;
import com.cs.fx.validation.MainValidator;

@Component
@Path("/")
public class ValidationEndpoint {
	
	@Autowired
	private MainValidator validator;
	
    @GET
    @Path("/status")
    @Produces(
    { MediaType.TEXT_PLAIN })
    public String status()
    {
        return "OK";
    }
    
	@POST
	@Path("/validate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<TradeValidationOutput> validate(List<TradeInput> transactionInputList) {
		return validator.validate(transactionInputList);
	}
}
