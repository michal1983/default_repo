package com.cs.fx.validation.validators;

import java.text.SimpleDateFormat;

import com.cs.fx.transaction.TradeValidationOutput;

public interface Validation {
	
	public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
	
	public void validate(TradeValidationOutput transactionOutput);

}
