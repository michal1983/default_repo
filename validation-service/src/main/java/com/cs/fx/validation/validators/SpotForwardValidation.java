package com.cs.fx.validation.validators;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cs.fx.transaction.TradeInput;
import com.cs.fx.transaction.TradeValidationOutput;
import com.cs.fx.validation.ValidationError;

@Component
public class SpotForwardValidation implements Validation{

	private static final List<String> SPOT_FORWARD_TYPE_VALUES = Arrays.asList("Spot", "Forward");

	
	@Override
	public void validate(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		//validation for types Spot and Forward we check if valueDate is empty
		if (SPOT_FORWARD_TYPE_VALUES.contains(tradeInput.getType()) && tradeInput.getValueDate() == null) {
			tradeOutput.setValid(false);
			tradeOutput.getErrors().add(new ValidationError("VALUEDATE_IS_NULL"));
		}
	}

}
