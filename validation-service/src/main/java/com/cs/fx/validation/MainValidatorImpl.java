package com.cs.fx.validation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.cs.fx.transaction.TradeInput;
import com.cs.fx.transaction.TradeValidationOutput;
import com.cs.fx.validation.validators.Validation;

@Component
public class MainValidatorImpl implements MainValidator {

	@Autowired
	@Qualifier(value="allValidation")
	private Validation allValidation;
	
	@Autowired
	@Qualifier(value="optionsValidation")
	private Validation optionsValidation;
	
	@Autowired
	@Qualifier(value="spotForwardValidation")
	private Validation spotForwardValidation;
	
	@Autowired
	@Qualifier(value="riskValidation")
	private Validation riskValidation;
	
	
	@Override
	public List<TradeValidationOutput> validate(List<TradeInput> transactionInputList) {
		
		List<TradeValidationOutput> validationResultList = new ArrayList<>();
		for(TradeInput transactionInput: transactionInputList)
		{
			TradeValidationOutput validationResult = new TradeValidationOutput(transactionInput);
			allValidation.validate(validationResult);
			optionsValidation.validate(validationResult);
			spotForwardValidation.validate(validationResult);
			riskValidation.validate(validationResult);
			
			validationResultList.add(validationResult);
		}
		return validationResultList;
	}

}
