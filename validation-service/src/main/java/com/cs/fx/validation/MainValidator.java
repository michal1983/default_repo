package com.cs.fx.validation;

import java.util.List;

import com.cs.fx.transaction.TradeInput;
import com.cs.fx.transaction.TradeValidationOutput;

public interface MainValidator 
{
	public List<TradeValidationOutput> validate(List<TradeInput> transactionInputList);	
}
