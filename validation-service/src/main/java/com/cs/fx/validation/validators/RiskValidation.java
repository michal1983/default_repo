package com.cs.fx.validation.validators;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.cs.fx.transaction.TradeInput;
import com.cs.fx.transaction.TradeValidationOutput;
import com.cs.fx.validation.ValidationError;

@Component
public class RiskValidation implements Validation {

	private static final String RISK_TYPE = "Risk";

	@Override
	public void validate(TradeValidationOutput tradeOutput) {

		validateTcnValueForRisk(tradeOutput);
		validateCcyPairForTrade(tradeOutput);

	}

	/**
	 * contains trade link (field TCN in risk and trade)
	 * @param tradeOutput
	 */
	private void validateTcnValueForRisk(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		if (RISK_TYPE.equals(tradeInput.getType()) && StringUtils.isEmpty(tradeInput.getTcn())) {
			tradeOutput.setValid(false);
			tradeOutput.getErrors().add(new ValidationError("TCN_IS_NULL_FOR_RISK_TYPE"));
		}
	}

	/**
	 * validate currency (contains in trade ccy pair)
	 * @param tradeOutput
	 */
	private void validateCcyPairForTrade(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		if (!RISK_TYPE.equals(tradeInput.getType()) && StringUtils.isEmpty(tradeInput.getCcyPair())) {
			tradeOutput.setValid(false);
			tradeOutput.getErrors().add(new ValidationError("CCYPAIR_IS_NULL"));
		}
	}

}
