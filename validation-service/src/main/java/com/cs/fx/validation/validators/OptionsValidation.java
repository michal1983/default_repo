package com.cs.fx.validation.validators;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.cs.fx.transaction.TradeInput;
import com.cs.fx.transaction.TradeValidationOutput;
import com.cs.fx.validation.ValidationError;

@Component
public class OptionsValidation implements Validation {

	private Logger logger = Logger.getLogger(OptionsValidation.class);

	private static final String VANILLA_OPTION_TYPE = "VanillaOption";
	private static final String AMERICAN_STYLE = "AMERICAN";
	private static final String EUROPEAN_STYLE = "EUROPEAN";
	private static final List<String> VANILLA_OPTION_STYLE = Arrays.asList(AMERICAN_STYLE, EUROPEAN_STYLE);

	@Override
	public void validate(TradeValidationOutput tradeOutput) {
		// in this validation we consider only VanillaOption
		if (!VANILLA_OPTION_TYPE.equals(tradeOutput.getTradeInput().getType())) {
			return;
		}

		validateOptionStyle(tradeOutput);
		validateExcerciseDate(tradeOutput);
		validateExpiryDate(tradeOutput);
	}

	/**
	 * the style can be either American or European
	 * @param tradeOutput
	 */
	private void validateOptionStyle(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		if (!VANILLA_OPTION_STYLE.contains(tradeInput.getStyle())) {
			tradeOutput.setValid(false);
			tradeOutput.getErrors()
					.add(new ValidationError("OPTION_STYLE_NOT_VALID" + tradeInput.getStyle()));
			return;
		}
	}

	/**
	 * American option style will have in addition the excerciseStartDate, which has
	 * to be after the trade date but before the expiry date
	 * 
	 * @param tradeOutput
	 */
	private void validateExcerciseDate(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		if (AMERICAN_STYLE.equals(tradeInput.getStyle())) {

			try {
				Date excerciseDate = FORMATTER.parse(tradeInput.getExcerciseStartDate());
				Date expireDate = FORMATTER.parse(tradeInput.getExpiryDate());
				Date tradeDate = FORMATTER.parse(tradeInput.getTradeDate());

				if (!excerciseDate.after(tradeDate)) {
					tradeOutput.setValid(false);
					tradeOutput.getErrors().add(new ValidationError("EXCERCISEDATE_HAS_TO_BE_AFTER_TRADE_DATE"));
				}

				if (!excerciseDate.before(expireDate)) {
					tradeOutput.setValid(false);
					tradeOutput.getErrors().add(new ValidationError("EXPIRYDATE_HAS_TO_BE_BEFORE_EXPIREDATE"));
				}
			} catch (ParseException e) {
				logger.error("Error during parsing date", e);
			}
		}
	}

	/**
	 * expiry date and premium date shall be before delivery date
	 * 
	 * @param tradeOutput
	 */
	private void validateExpiryDate(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		try {
			Date expireDate = FORMATTER.parse(tradeInput.getExpiryDate());
			Date premiumDate = FORMATTER.parse(tradeInput.getPremiumDate());
			Date deliveryDate = FORMATTER.parse(tradeInput.getDeliveryDate());

			if (!expireDate.before(deliveryDate)) {
				tradeOutput.setValid(false);
				tradeOutput.getErrors().add(new ValidationError("EXPIREDATE_HAS_TO_BE_BEFORE_DELIVERYDATE"));
			}

			if (!premiumDate.before(deliveryDate)) {
				tradeOutput.setValid(false);
				tradeOutput.getErrors().add(new ValidationError("PREMIUMDATE_HAS_TO_BE_BEFORE_DELIVERYDATE"));
			}
		} catch (ParseException e) {
			logger.error("Error during parsing date", e);
		}
	}
}
