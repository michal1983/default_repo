package com.cs.fx.validation.validators;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.cs.fx.transaction.TradeInput;
import com.cs.fx.transaction.TradeValidationOutput;
import com.cs.fx.validation.ValidationError;

@Component
public class AllValidation implements Validation {

	private Logger logger = Logger.getLogger(AllValidation.class);

	private static final List<String> SUPPORTED_COUNTER_PARTIES = Arrays.asList("PLUTO1", "PLUTO2");

	private static Set<String> AVAILABLE_CURRENCIES = new HashSet<>();

	{
		for(Currency currency : Currency.getAvailableCurrencies())
		{
			AVAILABLE_CURRENCIES.add(currency.getCurrencyCode());
		}
	}
	
	@Override
	public void validate(TradeValidationOutput tradeOutput) {

		validateTradeDate(tradeOutput);
		validateIfValueDateFallsOnWeekend(tradeOutput);
		validateCounterParties(tradeOutput);
		validateCcyPair(tradeOutput);
		validateCcy(tradeOutput);
	}

	/**
	 * value date cannot be before trade date
	 * 
	 * @param tradeOutput
	 */
	void validateTradeDate(TradeValidationOutput tradeOutput) {
		try {
			TradeInput tradeInput = tradeOutput.getTradeInput();
			if (StringUtils.isNotEmpty(tradeInput.getValueDate())
					&& StringUtils.isNotEmpty(tradeInput.getTradeDate())) {
				Date valueDate = FORMATTER.parse(tradeInput.getValueDate());
				Date tradeDate = FORMATTER.parse(tradeInput.getTradeDate());
				if (valueDate.before(tradeDate)) {
					tradeOutput.setValid(false);
					tradeOutput.getErrors().add(new ValidationError("VALUEDATE_BEFORE_TRADE_DATE"));
				}
			}
		} catch (ParseException e) {
			logger.error("Error during parsing VALUEDATE or TRADEDATE", e);
		}
	}

	/**
	 * value date cannot fall on weekend
	 * 
	 * @param tradeOutput
	 */
	void validateIfValueDateFallsOnWeekend(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		if (StringUtils.isNotEmpty(tradeInput.getValueDate())) {
			try {
				Date valueDate = FORMATTER.parse(tradeInput.getValueDate());
				if (isWeekend(valueDate)) {
					tradeOutput.setValid(false);
					tradeOutput.getErrors().add(new ValidationError("VALUEDATE_FALLS_ON_WEEKEND"));
				}
			} catch (ParseException e) {
				logger.error("Error during parsing VALUEDATE", e);
			}
		}
	}

	/**
	 * check whether date falls on weekend
	 * 
	 * @param date
	 * @return
	 */
	boolean isWeekend(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || (Calendar.DAY_OF_WEEK == Calendar.SUNDAY)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * if the counterparty is one of the supported ones
	 * 
	 * @param tradeOutput
	 */
	void validateCounterParties(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		if (StringUtils.isNotEmpty(tradeInput.getCustomer())) {
			if (!SUPPORTED_COUNTER_PARTIES.contains(tradeInput.getCustomer())) {
				tradeOutput.setValid(false);
				tradeOutput.getErrors()
						.add(new ValidationError("COUNTERPARTY_IS_NOT_SUPPORTED:" + tradeInput.getCustomer()));
			}
		}
	}

	/**
	 * validate currencies if they are valid ISO codes (ISO 4217)
	 * 
	 * @param tradeOutput
	 */
	void validateCcyPair(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		// ccyPair validation
		if (StringUtils.isNotEmpty(tradeInput.getCcyPair())) {
			if (tradeInput.getCcyPair().length() == 6) {
				String ccy1 = tradeInput.getCcyPair().substring(0, 3);
				String ccy2 = tradeInput.getCcyPair().substring(3);
				if (!AVAILABLE_CURRENCIES.contains(ccy1)) {
					tradeOutput.setValid(false);
					tradeOutput.getErrors().add(new ValidationError("CURRENCY_IS_NOT_VALID: " + ccy1));
				}
				if (!AVAILABLE_CURRENCIES.contains(ccy2)) {
					tradeOutput.setValid(false);
					tradeOutput.getErrors().add(new ValidationError("CURRENCY_IS_NOT_VALID: " + ccy2));
				}
			} else {
				tradeOutput.setValid(false);
				tradeOutput.getErrors()
						.add(new ValidationError("CCYPAIR_LENGTH_IS_NOT_CORRECT: " + tradeInput.getCcyPair()));
			}

		}

	}

	/**
	 * validate if ccy is correct currency
	 * 
	 * @param tradeOutput
	 */
	void validateCcy(TradeValidationOutput tradeOutput) {
		TradeInput tradeInput = tradeOutput.getTradeInput();
		// ccy validation
		if (StringUtils.isNotEmpty(tradeInput.getCcy())) {
			if (!AVAILABLE_CURRENCIES.contains(tradeInput.getCcy())) {
				tradeOutput.setValid(false);
				tradeOutput.getErrors()
						.add(new ValidationError("CURRENCY_IS_NOT_VALID: " + tradeInput.getCcy()));
			}
		}
	}

}
