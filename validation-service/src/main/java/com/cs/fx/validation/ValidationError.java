package com.cs.fx.validation;

/**
 * 
 * represents results of validation.
 *
 */

public class ValidationError {
	private String error;

	public ValidationError(String error) {
		super();
		this.error = error;
	}

	public String getError() {
		return error;
	}
}
